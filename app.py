from flask import Flask, render_template


app = Flask(__name__)


@app.route("/apple")
def apple():
    """
    Landing page.
    Should initialize Backend or fetch session.
    """
    data = "apple"

    return render_template("index.html", content=data)


@app.route("/google")
def google():
    """
    About page. Information about us.
    """
    data = "google"

    return render_template("index.html", content=data)



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)